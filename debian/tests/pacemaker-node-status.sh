#!/bin/sh

set -ex

DAEMON_TIMEOUT=60
CRM_TIMEOUT=5

# https://bugs.launchpad.net/bugs/1828228
ulimit -H -l unlimited 2>/dev/null || {
	echo "test disabled for unprivileged namespaces"
	exit 77
}

#
# daemons start
#

service corosync start
service pacemaker start
sleep $DAEMON_TIMEOUT

# Get the node name as sugested in
# https://lists.clusterlabs.org/pipermail/users/2022-May/030309.html
NODE="$(crm_node -n)"
if [ -z "$NODE" ]; then
  echo "Could not detect node name"
  exit 1
fi
# crmsh will try to ping the node name so make sure it can be resolved
if ! getent hosts "$NODE" >/dev/null 2>&1; then
  echo "127.0.0.1 $NODE" >> /etc/hosts
fi

#
# online
#

crm status | grep "Online:.*$NODE"

#
# standby
#

crm node standby $NODE
sleep $CRM_TIMEOUT
crm status | grep "Node $NODE: standby"

crm node online $NODE
sleep $CRM_TIMEOUT
crm status | grep "Online:.*$NODE"

#
# maintenance
#

crm node maintenance $NODE
sleep $CRM_TIMEOUT
crm status | grep "Node $NODE: maintenance"

crm node ready $NODE
sleep $CRM_TIMEOUT
crm status | grep "Online:.*$NODE"

#
# attributes
#

crm node attribute $NODE set memory_size 1024
crm node attribute $NODE show memory_size | grep 1024
crm node utilization $NODE set memory 2048
crm node utilization $NODE show memory | grep 2048
crm node server
crm node show

: INFO all tests OK
exit 0
